# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Enfant.id_enfant'
        db.add_column(u'data_enfant', 'id_enfant',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=64),
                      keep_default=False)
        for obj in orm.Enfant.objects.all():
            obj.id_endant = obj.id
            obj.save()


    def backwards(self, orm):
        # Deleting field 'Enfant.id_enfant'
        db.delete_column(u'data_enfant', 'id_enfant')


    models = {
        u'data.enfant': {
            'Meta': {'object_name': 'Enfant'},
            'date_naissance': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_enfant': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'prenom': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'sexe': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'})
        },
        u'data.facture': {
            'Meta': {'object_name': 'Facture'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_envoi_dernier_mail': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date_generation': ('django.db.models.fields.DateField', [], {}),
            'date_limite_paie': ('django.db.models.fields.DateField', [], {}),
            'date_reponse_tipi': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'debut_pge': ('django.db.models.fields.DateField', [], {}),
            'devise': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            'fin_pge': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'id_payeur_par_defaut': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'montant': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'montant_regle': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'nom_payeur': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'paye': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prelevement_automatique': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prenom_payeur': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'solde': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'statut_tipi': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'})
        },
        u'data.famille': {
            'Meta': {'object_name': 'Famille'},
            'appartement': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'batiment': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'btq': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'code_postal': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'code_secret': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'commune': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'complement_adresse': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'nombre_enfants': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'pays': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'personnes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['data.Personne']", 'through': u"orm['data.LiaisonParentFamille']", 'symmetrical': 'False'}),
            'qf_ccas': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'qf_vo': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'rue': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'voie': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'})
        },
        u'data.invoicenotificationemail': {
            'Meta': {'object_name': 'InvoiceNotificationEmail'},
            'date_sent': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_number': ('django.db.models.fields.CharField', [], {'max_length': '64'})
        },
        u'data.liaisonnameidfamille': {
            'Meta': {'unique_together': "(('name_id', 'famille'),)", 'object_name': 'LiaisonNameidFamille'},
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name_id': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'data.liaisonparentfamille': {
            'Meta': {'object_name': 'LiaisonParentFamille'},
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personne': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Personne']"})
        },
        u'data.personne': {
            'Meta': {'object_name': 'Personne'},
            'appartement': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'batiment': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'btq': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'cai': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'code_postal': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'commune': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'complement_adresse': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_id': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'numero_allocation': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'pays': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'prenom': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'regime': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'rue': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'sexe': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            'telephone_domicile': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'telephone_portable': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'telephone_portable_professionnel': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'voie': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['data']
