# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Personne'
        db.create_table(u'data_personne', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('prenom', self.gf('django.db.models.fields.CharField')(max_length=256, null=True, blank=True)),
            ('sexe', self.gf('django.db.models.fields.CharField')(max_length=1, null=True)),
            ('telephone_domicile', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('telephone_portable', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('numero_allocation', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('regime', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('cai', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('pays', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('code_postal', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, blank=True)),
            ('commune', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('voie', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('batiment', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('btq', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('appartement', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('rue', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('complement_adresse', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('name_id', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=256, null=True, blank=True)),
        ))
        db.send_create_signal(u'data', ['Personne'])

        # Adding model 'LiaisonParentFamille'
        db.create_table(u'data_liaisonparentfamille', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('famille', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['data.Famille'])),
            ('personne', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['data.Personne'])),
        ))
        db.send_create_signal(u'data', ['LiaisonParentFamille'])

        # Adding model 'LiaisonNameidFamille'
        db.create_table(u'data_liaisonnameidfamille', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name_id', self.gf('django.db.models.fields.CharField')(max_length=256, db_index=True)),
            ('famille', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['data.Famille'])),
            ('link_date', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'data', ['LiaisonNameidFamille'])

        # Adding unique constraint on 'LiaisonNameidFamille', fields ['name_id', 'famille']
        db.create_unique(u'data_liaisonnameidfamille', ['name_id', 'famille_id'])

        # Adding model 'Famille'
        db.create_table(u'data_famille', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('login', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('password', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('code_secret', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('qf_ccas', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('qf_vo', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('pays', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('code_postal', self.gf('django.db.models.fields.CharField')(max_length=16, null=True, blank=True)),
            ('commune', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('voie', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('batiment', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('btq', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('appartement', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('rue', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('complement_adresse', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('nombre_enfants', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'data', ['Famille'])

        # Adding model 'Facture'
        db.create_table(u'data_facture', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('famille', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['data.Famille'])),
            ('montant', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('devise', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('montant_regle', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('solde', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
            ('date_generation', self.gf('django.db.models.fields.DateField')()),
            ('date_limite_paie', self.gf('django.db.models.fields.DateField')()),
            ('prenom_payeur', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('nom_payeur', self.gf('django.db.models.fields.CharField')(max_length=128)),
            ('prelevement_automatique', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('debut_pge', self.gf('django.db.models.fields.DateField')()),
            ('fin_pge', self.gf('django.db.models.fields.DateField')()),
            ('paye', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('active', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'data', ['Facture'])

        # Adding model 'Enfant'
        db.create_table(u'data_enfant', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('famille', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['data.Famille'])),
            ('prenom', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('nom', self.gf('django.db.models.fields.CharField')(max_length=128, null=True, blank=True)),
            ('sexe', self.gf('django.db.models.fields.CharField')(max_length=1, null=True)),
            ('date_naissance', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'data', ['Enfant'])


    def backwards(self, orm):
        # Removing unique constraint on 'LiaisonNameidFamille', fields ['name_id', 'famille']
        db.delete_unique(u'data_liaisonnameidfamille', ['name_id', 'famille_id'])

        # Deleting model 'Personne'
        db.delete_table(u'data_personne')

        # Deleting model 'LiaisonParentFamille'
        db.delete_table(u'data_liaisonparentfamille')

        # Deleting model 'LiaisonNameidFamille'
        db.delete_table(u'data_liaisonnameidfamille')

        # Deleting model 'Famille'
        db.delete_table(u'data_famille')

        # Deleting model 'Facture'
        db.delete_table(u'data_facture')

        # Deleting model 'Enfant'
        db.delete_table(u'data_enfant')


    models = {
        u'data.enfant': {
            'Meta': {'object_name': 'Enfant'},
            'date_naissance': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'prenom': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'sexe': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'})
        },
        u'data.facture': {
            'Meta': {'object_name': 'Facture'},
            'active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'date_generation': ('django.db.models.fields.DateField', [], {}),
            'date_limite_paie': ('django.db.models.fields.DateField', [], {}),
            'debut_pge': ('django.db.models.fields.DateField', [], {}),
            'devise': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            'fin_pge': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'montant': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'montant_regle': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'nom_payeur': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'paye': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prelevement_automatique': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'prenom_payeur': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'solde': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'})
        },
        u'data.famille': {
            'Meta': {'object_name': 'Famille'},
            'appartement': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'batiment': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'btq': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'code_postal': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'code_secret': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'commune': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'complement_adresse': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'login': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'nombre_enfants': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'pays': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'personnes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['data.Personne']", 'through': u"orm['data.LiaisonParentFamille']", 'symmetrical': 'False'}),
            'qf_ccas': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'qf_vo': ('django.db.models.fields.CharField', [], {'max_length': '128', 'null': 'True', 'blank': 'True'}),
            'rue': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'voie': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'})
        },
        u'data.liaisonnameidfamille': {
            'Meta': {'unique_together': "(('name_id', 'famille'),)", 'object_name': 'LiaisonNameidFamille'},
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'link_date': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'name_id': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'data.liaisonparentfamille': {
            'Meta': {'object_name': 'LiaisonParentFamille'},
            'famille': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Famille']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'personne': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['data.Personne']"})
        },
        u'data.personne': {
            'Meta': {'object_name': 'Personne'},
            'appartement': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'batiment': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'btq': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'cai': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'code_postal': ('django.db.models.fields.CharField', [], {'max_length': '16', 'null': 'True', 'blank': 'True'}),
            'commune': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'complement_adresse': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name_id': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'nom': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'numero_allocation': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'pays': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'prenom': ('django.db.models.fields.CharField', [], {'max_length': '256', 'null': 'True', 'blank': 'True'}),
            'regime': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'rue': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            'sexe': ('django.db.models.fields.CharField', [], {'max_length': '1', 'null': 'True'}),
            'telephone_domicile': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'telephone_portable': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'voie': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['data']