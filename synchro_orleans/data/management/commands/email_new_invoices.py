# -*- coding: utf-8 -*-

from optparse import make_option
from datetime import datetime, timedelta
import json
import shutil
import os
import requests
import logging
from hashlib import sha256

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings
from django.utils import timezone
from django.db.models import Q
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives

from synchro_orleans.data.models import Facture, InvoiceNotificationEmail
from synchro_orleans import signature

notification_timeout = settings.INVOICES_NOTIFICATION_TIMEOUT
invoice_view_url_base = settings.INVOICE_VIEW_URL_BASE

logger = logging.getLogger(__name__)

class Command(BaseCommand):

    help = """
    Sends email notifications about the invoices
    """

    email_subject = 'Votre facture est disponible sur le Portail Famille'
    email_from = settings.DEFAULT_FROM_EMAIL
    secret = settings.INVOICE_HASHING_SECRET

    option_list = BaseCommand.option_list + (
        make_option('--list',
                    action='store_true',
                    default=False,
                    help='List outgoing emails in CSV format'),
        make_option('--fake',
                    action='store_true',
                    default=False,
                    help='Fake emailing'),
        make_option('--id',
                    action='store_true',
                    default=None,
                    help='Invoice\'s id to be emailed'),
        make_option('--to',
                    action='store_true',
                    default=None,
                    help='Fake emailing destination'),
        )


    def get_invoice_hash(self, *args):
        msg = ''.join(map(lambda s: str(s), args))
        return sha256(msg).hexdigest()[:7]

    def handle(self, *args, **options):
        if options['list']:
            print "destination;nr. facture;montant;montant regle"

        queryset = Facture.objects.filter(active=True, date_limite_paie__gte=datetime.now(),
                                          famille__liaisonnameidfamille__isnull=False).exclude(Q(paye=True) | Q(date_reglement__isnull=False))
        if options['fake']:
            if not options['id']:
                raise CommandError('An invoice id should be provided')
            if not options['to']:
                raise CommandError('A destination email should be provided')
            queryset = queryset.filter(pk=options['id'])

        for invoice in queryset:
            try:
                notification = InvoiceNotificationEmail.objects.filter(invoice_number=invoice.id).latest()
                if notification.date_sent > timezone.now() - timedelta(days=notification_timeout):
                    continue
                if invoice.solde < 1:
                    continue
            except InvoiceNotificationEmail.DoesNotExist:
                pass

            nameid = invoice.famille.liaisonnameidfamille_set.all()[0]
            signed_query = signature.sign_query('orig=synchro-orleans', settings.EMAILING_APIKEY)
            url = '{url}/{nameid}?{query}'.format(url=settings.IDP_USER_INFO_URL,
                                                  nameid=nameid.name_id,
                                                  query=signed_query)
            logger.debug('requesting: %s' % url)
            response = requests.get(url)
            try:
                data = response.json()['data']
                if not data:
                    raise ValueError
            except (KeyError, ValueError):
                continue
            attachment = os.path.join(settings.INVOICES_DIR, 'facture_%s.pdf' % invoice.id)
            invoice_hash = self.get_invoice_hash(invoice.id, invoice.date_generation)

            context = {'invoice': invoice, 'invoice_view_url_base': invoice_view_url_base,
                       'invoice_hash': invoice_hash}
            context.update(data)

            if invoice.prelevement_automatique:
                email_template = 'autobilling_invoice_mail.txt'
            elif invoice.solde == 0:
                email_template = 'null_invoice_mail.txt'
            elif invoice.solde < 1:
                email_template = 'sub1_invoice_mail.txt'
            else:
                email_template = 'invoice_mail.txt'

            if options['list']:
                print "{email};{invoice.id};{invoice.montant};{invoice.montant_regle}".format(**context)
                continue

            context = Context(context)
            text_body = get_template(email_template).render(context)
            if options['fake']:
                to = [options['to']]
            else:
                to = [data['email']]
            message = EmailMultiAlternatives(self.email_subject, text_body, self.email_from, to)
            message.attach_file(os.path.join(settings.INVOICES_DIR, 'facture_%s.pdf'% invoice.id))
            message.send()
            logger.debug('email for invoice nr. %s sent' % invoice.id)

            invoice.date_envoi_dernier_mail = timezone.now()
            InvoiceNotificationEmail.objects.create(invoice_number=invoice.id)
            invoice.save()
