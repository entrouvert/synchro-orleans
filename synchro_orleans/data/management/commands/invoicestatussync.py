from os import path
from datetime import date

from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from synchro_orleans.data.models import Facture

class Command(BaseCommand):

    help = """Synchronizes the invoices state: if a file corresponding to a
invoice is present the invoice becomes active and inactive if not."""

    def handle(self, *args, **options):

        for invoice in Facture.objects.all():
            active = path.exists(settings.INVOICES_LOCATION_PATTERN.format(invoice_id = invoice.id))
            active = active and invoice.date_generation <= date.today()

            if active != invoice.active:
                invoice.active = active
                invoice.save()

