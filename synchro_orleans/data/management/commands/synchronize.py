from django.core.management.base import BaseCommand, CommandError
from django.db.models import get_model
from django.conf import settings

from optparse import make_option
from os import path, mkdir
from glob import glob
from shutil import rmtree
from subprocess import call
from zipfile import ZipFile, is_zipfile

exports_names_map = {'extract_prcit_personne.csv': 'personne.csv',
                     'extract_prcit_famille.csv': 'famille.csv',
                     'extract_prcit_enfant.csv': 'enfant.csv',
                     'extract_prcit_facture.csv': 'facture.csv'
                     }

INPUT_ENCODING = 'ISO-8859-1'
OUTPUT_ENCODING = 'UTF-8'

class Command(BaseCommand):
    help = 'Synchronises the database with CSV the exports from Concerto'

    option_list = BaseCommand.option_list + (
        make_option('-s', '--source', dest = 'source',
                    help = """directory containing the source CSV files.
Should also contain a subdirectory called 'pdf' with the files respecting the
pattern 'facture-xxx.pdf', where 'xxx' is the invoice number
"""
                     ),
        )

    option_list = option_list + (
        make_option('-d', '--database', dest = 'database',
                    help = 'database the data will be imported into',
                    ),
        )

    def handle(self, *args, **options):

        source = options['source']
        database = options['database']

        if database:
            settings.DATABASES['default']['NAME'] = database

        if is_zipfile(source):
            with ZipFile(source) as data:
                dest = path.join('/tmp', path.basename(source))
                if not path.exists(dest):
                    mkdir(dest)
                for content in data.namelist():
                    data.extract(content, dest)
                    export = path.join(dest, exports_names_map[content])
                    unencoded_file = path.join(dest, content)
                    call(['iconv', '-f', INPUT_ENCODING, '-t', OUTPUT_ENCODING,
                          unencoded_file, '-o', export])

                for source_file in map(lambda f: path.join(dest, '%s.csv' % f),
                                       ('personne', 'famille', 'enfant', 'facture')):
                    file_name = path.basename(source_file)
                    name, ext = path.splitext(file_name)
                    model = get_model('data', name)
                    if model:
                        model.objects.synchronize(source_file)
                    else:
                        self.stdout.write('there is no model corresponding to file %s' % file_name)
                rmtree(dest)
        else:
            raise CommandError('the file %s is not a zip archive or doesn\'t exist' % source)




