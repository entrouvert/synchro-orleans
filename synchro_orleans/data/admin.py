from django.contrib import admin

from models import Personne, Famille, Facture, Enfant, LiaisonNameidFamille
from models import InvoiceNotificationEmail


class PersonnesInline(admin.TabularInline):
    model = Famille.personnes.through
    verbose_name_plural = u'Parents'
    raw_id_fields = ('personne',)

class KidsInline(admin.TabularInline):
    model = Enfant
    verbose_name_plural = u'Enfants'
    extra = 2

class FamilleAdmin(admin.ModelAdmin):
    inlines = [PersonnesInline, KidsInline]
    exclude = ('personnes', )

admin.site.register(Famille, FamilleAdmin)


class PersonneAdmin(admin.ModelAdmin):
    search_fields = ['id', 'nom', 'prenom']
    list_display = ('id', 'nom', 'prenom', 'sexe')
    list_filter = ['sexe']

admin.site.register(Personne, PersonneAdmin)


class EnfantAdmin(admin.ModelAdmin):
    search_fields = ['id_enfant', 'nom', 'prenom']
    list_display = ('id_enfant', 'nom', 'prenom', 'sexe', 'date_naissance', 'famille')
    list_filter = ['sexe', 'date_naissance']
    raw_id_fields = ('famille',)

admin.site.register(Enfant, EnfantAdmin)

class LiaisonNameidFamilleAdmin(admin.ModelAdmin):
    list_display = ('name_id', 'famille', 'link_date')
    search_fields = ('name_id',)
    raw_id_fields = ('famille', )

class FactureAdmin(admin.ModelAdmin):
    list_display = ('id', 'date_generation', 'date_limite_paie', 'prelevement_automatique', 'famille', 'montant', 'solde', 'active',)
    list_filter = ['prelevement_automatique', 'date_generation', 'date_limite_paie', 'paye', 'active']
    raw_id_fields = ('famille',)

class NotificationsAdmin(admin.ModelAdmin):
    list_display = ('invoice_number', 'date_sent')
    list_filter = ['invoice_number']

admin.site.register(Facture, FactureAdmin)
admin.site.register(InvoiceNotificationEmail, NotificationsAdmin)
admin.site.register(LiaisonNameidFamille, LiaisonNameidFamilleAdmin)
